# Prueba Tecnica FRONT END

En este repo se encuentra un express server que expone servicios para el CRUD de hoteles.

## Ejecutar Proyecto

En primera instancia se deben descargar las dependencias, esto se hace por medio de **npm install**.
Se crearon 3 scripts de inicio para el servidor, sirven para desplegar el servidor en ambiente local, develop y producción.

* npm run startLocal
* npm run startDev
* npm run startProd

## mongoDB

La Api Rest necesita de mongodb para ejecutarse ya que la capa de persistencia esta creada en una base de datos de mongo.
Para ejecutrar mongodb se debe correr el comando: **sudo service mongod start**
