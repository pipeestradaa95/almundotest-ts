import {Request, Response} from "express";
import { HotelController } from "../app/hotels/hotelsController";

export class Routes {

  public hotelController: HotelController = new HotelController()
    public routes(app): void {
      app.route('/api/hotels')
      .get((req,res) => this.hotelController.findAllHotels(req,res))
      .post((req,res) => this.hotelController.addHotels(req,res))
      .put((req,res) => this.hotelController.findByFilters(req,res));
;

      app.route('/api/hotel')
      .post((req,res) => this.hotelController.addHotel(req,res));

      app.route('/api/hotel/:hotelId')
      .get((req,res) => this.hotelController.findById(req,res))
      .put((req,res) => this.hotelController.updateHotel(req,res))
      .delete((req,res) => this.hotelController.deleteHotel(req,res));
    }
}
