import * as express from "express";
import * as bodyParser from "body-parser";
import { Routes } from "./routes/appRoutes";
import * as mongoose from "mongoose";
import { ServerConf, Configuration } from "../config/configuration"
class App {

    public app: express.Application;
    public routePrv: Routes = new Routes();
    public mongoUrl: string = 'mongodb://localhost/almundoTest';
    private configuration: Configuration;

    constructor() {
        this.app = express();
        this.config();
        this.mongoSetup();
        this.routePrv.routes(this.app);

        this.app.listen(this.configuration.config.port, () => {
            console.log('Express server listening on port ' + this.configuration.config.port);
        })
    }

    private config(): void{
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.configuration = new Configuration();
        let allowOrigin = this.configuration.config.allowOrigin;
        // Add headers
        this.app.use(function (req, res, next) {

            // Website you wish to allow to connect
            res.setHeader('Access-Control-Allow-Origin', allowOrigin);

            // Request methods you wish to allow
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

            // Request headers you wish to allow
            res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

            // Set to true if you need the website to include cookies in the requests sent
            // to the API (e.g. in case you use sessions)
            res.setHeader('Access-Control-Allow-Credentials', []);

            // Pass to next layer of middleware
            next();
        });

    }

    private mongoSetup(): void{
      mongoose.Promise = global.Promise;
      mongoose.connect(this.mongoUrl);
    }

}

export default new App().app;
