import * as mongoose from 'mongoose';
import { HotelSchema } from '../models/hotelSchema';
mongoose.Promise = Promise;



export class HotelsQueriesServices{
  Hotel = mongoose.model('Hotels', HotelSchema);

  public findAllHotels(): Promise<any> {
    return this.Hotel.find();
  }

   public findById(id: string): Promise<any> {
    console.log('GET /hotel/'+id);
    return this.Hotel.findOne({"id" : id});
  }

  public findByFilters(name: string, starsArray: Array<number>): Promise<any>{
  	console.log('PUT /hotel');
    let _name: string;
  	if(name === undefined){ _name = "" } else { _name = name }

    let _starsArray: Array<number>;

  	if(starsArray === undefined){ _starsArray = [1,2,3,4,5]	} else { _starsArray = starsArray }
  	return this.Hotel.find({
  		name: {$regex : _name, $options: 'i'},
  		stars: { $in: _starsArray }
  	});
  };
}
