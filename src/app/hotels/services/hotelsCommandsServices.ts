import * as mongoose from 'mongoose';
import { HotelSchema } from '../models/hotelSchema';
mongoose.Promise = Promise;



export class HotelsCommandsServices{
  Hotel = mongoose.model('Hotels', HotelSchema);

  public findAllHotels(): Promise<any> {
    return this.Hotel.find();
  }

  public addHotel(hotelReq: any): Promise<any> {
  	console.log('POST /hotel');
  	return new this.Hotel(hotelReq).save();
  };

  public addHotels(hotelsReq: any): Promise<any> {
    console.log('POST /hotels');
    let HotelCreator = this.Hotel;
    hotelsReq.forEach(function(element) {
      let hotel = new HotelCreator(element);
      hotel.save();
    });

    return new Promise<string>((resolve, reject) => {
      resolve('Registros ingresados con exito');
    });
  };

  public updateHotel(hotelId: string, hotelReq: any): Promise<any> {
    console.log('PUT /hotel/' + hotelId);
    let query = {"id" : hotelId};

    let update = {
  		id:           hotelReq.id,
  		name: 	      hotelReq.name,
  		stars:        hotelReq.stars,
  		price:        hotelReq.price,
  		image:        hotelReq.image,
  		amenities:    hotelReq.amenities
  	} ;
  	return this.Hotel.findOneAndUpdate(query,{$set : update});
  };

  public deleteHotel(hotelId: string): Promise<any> {
    console.log('DELETE /hotel/' + hotelId);
    let query = {"id" : hotelId};
  	return this.Hotel.findOneAndRemove(query);

    /*return new Promise<string>((resolve, reject) => {
      resolve('Hotel eliminado con exito');
    });*/
  };


}
