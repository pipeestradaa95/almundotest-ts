import * as mongoose from 'mongoose';
import { HotelSchema } from './models/hotelSchema';
import { Request, Response } from 'express';
import { HotelsQueriesServices } from './services/hotelsQueriesServices';
import { HotelsCommandsServices } from './services/hotelsCommandsServices';


export class HotelController {

  hotelsQueriesServices: HotelsQueriesServices;
  hotelsCommandsServices: HotelsCommandsServices;
  constructor(){
    this.hotelsQueriesServices = new HotelsQueriesServices();
    this.hotelsCommandsServices = new HotelsCommandsServices();
  }

  //GET - Return all hotels in the DB
  public findAllHotels(req: Request, res: Response){
    this.hotelsQueriesServices.findAllHotels().then(list => {
      res.status(200).json(list);
    }, error => {
      res.status(500).json(error.message);
    });
  }

  //GET - Return a hotel with specified ID
  public findById(req: Request, res: Response){
    console.log("req.body",req.params.hotelId);
  	this.hotelsQueriesServices.findById(req.params.hotelId).then( hotel => {
      res.status(200).json(hotel);
    }, error => {
      res.status(500).json(error.message);
    });
  };

  //GET - Get Hotels by filters
  public findByFilters(req: Request, res: Response){
  	this.hotelsQueriesServices.findByFilters(req.body.name, req.body.stars).then(list => {
      res.status(200).json(list);
    }, error => {
      res.status(500).json(error.message);
    });
  };

  //POST - Insert a new hotel in the DB
  public addHotel(req: Request, res: Response){
  	this.hotelsCommandsServices.addHotel(req.body).then( hotel => {
      res.status(200).json(hotel);
    }, error => {
      res.status(500).json(error.message);
    });
  };

  //POST - Insert a list of new hotel in the DB
  public addHotels(req: Request, res: Response){
  	this.hotelsCommandsServices.addHotels(req.body).then( exito => {
      res.status(200).json(exito);
    }, error => {
      res.status(500).json(error.message);
    });
  };

  //PUT - Update a hotel that already exists
  public updateHotel(req: Request, res: Response){
  	this.hotelsCommandsServices.updateHotel(req.params.hotelId,req.body).then( hotel => {
      if(hotel === null){
        res.status(404).json("Hotel no encontrado");
      }else{
        res.status(200).json(req.body);
      }
    }, error => {
      res.status(500).json(error.message);
    });
  };

  //DELETE - Delete a hotel that already exists
  public deleteHotel(req: Request, res: Response){
  	this.hotelsCommandsServices.deleteHotel(req.params.hotelId).then( exito => {
      res.status(200).json("Hotel eliminado con exito");
    }, error => {
      res.status(500).json(error.message);
    });
  };
}
