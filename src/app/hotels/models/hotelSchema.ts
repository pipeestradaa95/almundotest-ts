import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const HotelSchema = new Schema({
  id:       { type: String },
  name:     { type: String },
  stars:    { type: Number },
  price:    { type: Number },
  image:    { type: String },
  amenities:{ type: Array }
});
