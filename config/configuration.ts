
export interface ServerConf{
  port: number;
  allowOrigin: string;
}

export class Configuration{
  public config: ServerConf = null;

  constructor(){
    const NODE_ENV = process.env.NODE_ENV;
    switch (NODE_ENV) {
      case 'production':
        this.config = require('./production.json');
        break;
      case 'develop':
        this.config = require('./develop.json');
        break;
      default:
        this.config = require('./local.json');
    }
  }
}
